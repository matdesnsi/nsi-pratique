R = [None, 1]
S = [None, 2]
...



# tests

assert R[3] == 7
assert S[3] == 5
assert (len(R) >= 10000) and (len(S) >= 10000)
