R = [None, 1, 3]
S = [None, 2, 4, 5, 6]
i_r = 2  # l'indice du dernier élément de R
r_suivant = 7
while i_r < 10000:
    R.append(r_suivant)
    i_r += 1
    prochain = R[i_r] + S[i_r]
    if len(S) < 10000:
        for s in range(r_suivant + 1, prochain):
            S.append(s)
    r_suivant = prochain


# tests

assert R[3] == 7
assert S[3] == 5
assert (len(R) >= 10000) and (len(S) >= 10000)
