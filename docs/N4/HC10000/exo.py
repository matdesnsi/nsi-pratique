a = [None, 1, 1]
S = [None, 1, 2]
...



# tests

assert S[1] == 1
assert S[2] == 2
assert S[3] == 4
assert S[4] == 6
assert len(S) >= 10000
