def liste_evenements(trains):
    evenements = []
    for (arrivee, depart) in trains:
        evenements.append((arrivee, +1))
        evenements.append((depart, -1))
    return evenements


def nb_min_quais(trains):
    evenements = liste_evenements(trains)
    evenements.sort()  # tri de la liste des évènements
    # en cas d'égalité, les départs sont en premiers

    nb_quais_occupes = 0
    nb_mini = 0
    for (horaire, variation) in evenements:
        nb_quais_occupes += variation
        if nb_quais_occupes > nb_mini:
            nb_mini = nb_quais_occupes
    return nb_mini


# Tests
trains = [(3, 5), (2, 4), (6, 8)]
assert nb_min_quais(trains) == 2

trains = [(1, 5), (6, 7), (5, 5.99)]
assert nb_min_quais(trains) == 1

trains = [(1, 2), (2, 3), (3, 4), (4, 5)]
assert nb_min_quais(trains) == 1

trains = []
assert nb_min_quais(trains) == 0
