# Commentaires

{{ IDE('exo_corr') }}

Prenons un exemple : afin de calculer la somme des chiffres de $409$ on doit additionner le chiffre des unités $9$, celui des dizaines $0$ et celui des centaines $4$.

Le chiffre des unités vaut $9$. C'est le reste de la division de $409$ par $10$. On l'obtient en Python en faisant `409 % 10`.

Ce calcul effectué, on calcule le quotient de $409$ par $10$ : on obtient $40$. On l'obtient en Python en faisant `409 // 10`.

On peut alors recommencer la démarche (reste de la division suivie de quotient) jusqu'à ce que le quotient soit nul.

|Etape|Valeur de $n$|Reste de la division par $10$|Quotient de la division par $10$|
|:-:|:-:|:-:|:-:|
|1|$409$|$9$|$40$|
|2|$40$|$0$|$40$|
|3|$4$|$4$|$0$|

Le fonction `somme_chiffres` est récursive. Le *cas de base* est atteint lorsque l'argument `n` est nul. On renvoie alors `0`.

Dans les autres cas (`n` strictement positif), on renvoie le chiffre des unités de `n` additionnés à la somme des chiffres du quotient de `n` par `10`.

Pour $409$ on fait donc :

$$\begin{align*} 
somme\_chiffres(409) &=  9 + somme\_chiffres(40) \\ 
 &=  9 + 0 + somme\_chiffres(4) \\
 &= 9 + 0 + 4\\
 &=13\\
\end{align*}$$

La fonction `harshad` est simple : on se contente de tester si le reste de la division de `n` par `somme_chiffres(n)` est égal à `0`. Si oui, le nombre est *harshad*, si non il ne l'est pas.