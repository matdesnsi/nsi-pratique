def deux_meilleurs(scores):
    max1, max2 = None, None
    for v in scores:
        if max1 is None or ...:
            max1, max2 = ..., ...
        elif ... or ...:
            ...
    return ..., ...

# tests
scores_1 = [13, 15, 12, 3, 19, 7, 14]
assert deux_meilleurs(scores_1) == (19, 15)
scores_2 = [27, 35, 14, 35, 3, 26, 9]
assert deux_meilleurs(scores_2) == (35, 35)
assert deux_meilleurs([4, 5]) == (5, 4)
assert deux_meilleurs([5, 4]) == (5, 4)
assert deux_meilleurs([5, 5]) == (5, 5)
assert deux_meilleurs([9]) == (9, None)
assert deux_meilleurs([]) == (None, None)
