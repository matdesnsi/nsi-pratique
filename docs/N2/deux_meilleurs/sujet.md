---
author: Guillaume CONNAN et Romain JANVIER
title: Deux meilleurs
tags:
  - 1-boucle
---

# Deux meilleurs scores

Vous venez de développer *Flap Flap Bird*, un jeu très dur où il faut
aider un oiseau à éviter des obstacles. À la fin de la partie, vous
voulez afficher les deux meilleurs scores du joueur. Ces deux meilleurs
scores peuvent être égaux.

On considère les deux listes de scores suivants :
```python
scores_1 = [13, 15, 12, 3, 19, 7, 14]
scores_2 = [27, 35, 14, 35, 3, 26, 9]
```
Pour `score_1`, les deux meilleurs scores sont `19` et `14`.
Pour `score_2`, ce sont `35` et `35`.

Si la liste de scores ne contient pas assez de valeurs, les meilleurs
valeurs manquantes sont remplacées par `None`.

Compléter le code de la fonction `deux_meilleurs` qui prend un liste
d'entiers `scores` et renvoie le couple formé par les deux meilleurs valeurs `max1` et `max2`, avec `max1`≥`max2` si aucun des deux ne vaut `None`.

!!! warning "Contraintes"

    On n'utilisera ni `max`, ni `min`, ni `sort`, ni `sorted`.

??? info "La valeur `None`"

    `None` est une valeur spéciale utilisée en Python et qui correspond
    à un manque de valeur *normale*. Lorsque l'exécution d'une fonction
    se termine sans avoir rencontré un `return`, Python renvoie
    automatiquement la valeur `None`.
    
    On peut tester si une variable `x` contient la valeur `None` de la
    manière suivante :
    ```python
    if x is None:
        ...
    ```
    On pourrait également tester `x == None` mais si `x` est un objet
    d'une classe dans laquelle l'égalité a été redéfinie, l'égalité peut
    être vraie, alors que `x is None` ne l'est pas.

??? tip "Indications"

    L'algorithme fonctionne de la manière suivante :

    * On parcourt toutes les valeurs de la liste.
    * À chaque nouvelle valeur `v` on étudie les cas suivants :
        * Si `max1` n'est pas définie ou si `v` > `max1`, on met à jour
          `max1` et `max2`.
        * Sinon, si `max2` n'est pas définie ou si `v` est plus grande,
          on met à jour `max2`.

!!! example "Exemples"

    ```pycon
    >>> scores_1 = [13, 15, 12, 3, 19, 7, 14]
    >>> deux_meilleurs(score_1)
    (19, 15)
    >>> scores_2 = [27, 35, 14, 35, 3, 26, 9]
    >>> deux_meilleurs(score_2)
    (35, 35)
    >>> deux_meilleurs([4, 5])
    (5, 4)
    >>> deux_meilleurs([5, 4])
    (5, 4)
    >>> deux_meilleurs([5, 5])
    (5, 5)
    >>> deux_meilleurs([9])
    (9, None)
    >>> deux_meilleurs([])
    (None, None)
    ```

{{ IDE('exo') }}
