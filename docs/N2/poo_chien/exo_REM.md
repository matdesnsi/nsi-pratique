## Commentaires

{{ IDE('exo_corr') }}

Les trois premières méthodes `__init__` ne présentent pas de difficultés. On notera cependant l'utilisation de l'argument `self` qui permet d'indiquer que cette méthode s'applique à l'objet concerné par l'appel. Toutefois lors de l'utilisation de ces méthodes on omet ce paramètre : `chien.donne_nom()` par exemple

Les trois autre méthodes (`machouille`, `aboie` et `mange`) prennent deux arguments : `self` (ignoré là encore lors des appels) et un paramètre à fournir lors de l'appel.

Dans le cas de la méthode `mange`, on vérifie que `ration` satisfait les conditions à l'aide d'un test `if 0 < ration <= self.poids / 10:`. On pourrait aussi utiliser une assertion `assert 0 < ration <= self.poids / 10:`. Toutefois cette façon de procéder génèrerait une erreur si la condition n'est pas respectée. L'utilisation du test classique permet de gérer précisément les deux cas de figure et de renvoyer `False` en cas d'échec du test.