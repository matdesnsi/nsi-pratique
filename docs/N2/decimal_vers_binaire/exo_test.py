# tests

assert binaire(0) == '0'
assert binaire(1) == '1'
assert binaire(16) == '10000'
assert binaire(77) == '1001101'

# autres tests

assert binaire(999) == '1111100111'
assert binaire(1234567890) == '1001001100101100000001011010010'
assert binaire(4) == '100'
assert binaire(256) == '100000000'
assert binaire(255) == '11111111'
