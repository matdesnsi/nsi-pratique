def tri_insertion(tableau):
    n = len(tableau)
    for i in range(1, n):
        j = i
        valeur_a_inserer = tableau[i]
        while j > 0 and valeur_a_inserer < tableau[j - 1]:
            tableau[j] = tableau[j - 1]
            j = j - 1
        tableau[j] = valeur_a_inserer
