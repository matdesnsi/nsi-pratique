---
author: Romain Janvier, Sébastien Hoarau, Nicolas Revéret
title: Tri par insertion
tags:
    - tri
    - liste
---

La méthode du tri par insertion repose sur un double parcours de la liste à trier :

- un parcours de gauche à droite commençant au deuxième élément avec un indice $i$. On a la garantie que la liste jusqu'à l'indice $i$ exclu est triée ;
- un parcours de droite à gauche du début de la liste jusqu'à l'indice $i$ pour y insérer à la bonne place l'élément d'indice $i$. Ce parcours utilise un indice $j$.

![tri par insertion](images/insertion.svg)

La fonction `tri_insertion` suivante prend en argument un tableau de nombres `tableau` et le trie dans l'ordre croissant en utilisant cette méthode.

Il s'agit d'un tri en place ce qui signifie que le tableau passé en paramètre sera directement modifié. Il est inutile de le renvoyer.

Compléter la fonction pour qu'elle réponde à la spécification demandée.

???+ example "Exemples"

    ```pycon
    >>> tableau_0 = [9, 5, 8, 7, 6]
    >>> tri_insertion(tableau_0)
    >>> tableau_0
    [5, 6, 7, 8, 9]
    ```
    ```pycon
    >>> tableau_1 = [2, 5, -1, 7, 0, 28]
    >>> tri_insertion(tableau_1)
    >>> tableau_1
    [-1, 0, 2, 5, 7, 28]
    ```

    ```pycon
    >>> un_seul = [9]
    >>> tri_insertion(un_seul)
    >>> un_seul
    [9]
    ```

    ```pycon
    >>> tableau_vide = []
    >>> tri_insertion(tableau_vide)
    >>> tableau_vide
    []
    ```
{{ IDE('exo') }}
