---
author: BNS2022-38.2 puis Sébastien Hoarau
title: Jeu "plus ou moins"
tags:
    - boucle
    - while
    - diviser-régner
---

Le jeu du « plus ou moins » consiste à deviner un nombre entier choisi entre 1 et 99.
Un élève de NSI décide de le coder en langage Python de la manière suivante :

- le programme génère un nombre entier aléatoire compris entre 1 et 99 ;
- si la proposition de l'utilisateur est plus petite que le nombre cherché, l'utilisateur en est averti. Il peut alors en tester un autre ;
- si la proposition de l'utilisateur est plus grande que le nombre cherché, l'utilisateur en est averti. Il peut alors en tester un autre ;
- si l'utilisateur trouve le bon nombre en 10 essais ou moins, il gagne ;
- si l'utilisateur a fait plus de 10 essais sans trouver le bon nombre, il perd.

!!! note "Note"

    La fonction `randint` est utilisée. Si `a` et `b` sont des entiers, `randint(a, b)` renvoie un nombre entier compris entre `a` et `b`, incluant les **deux bornes**.

Compléter le code fourni dans l'IDE et le tester :

{{ IDE('exo') }}

