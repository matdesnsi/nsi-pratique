# Tests
assert compression_RLE('aabbbbcaa') == '2a,4b,1c,2a'
assert compression_RLE('aa aa') == '2a,1 ,2a'
assert compression_RLE('aaa') == '3a'
assert compression_RLE('aA') == '1a,1A'


# Tests supplémentaires
assert compression_RLE('r'*10) == '10r'
assert compression_RLE('r'*10+'a'*5) == '10r,5a'
assert compression_RLE(compression_RLE('r'*10+'a'*5)) == '11,10,1r,1,,15,1a'
