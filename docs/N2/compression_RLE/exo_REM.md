# Commentaires

On crée la variable `chaine_comrpessee` destinée à contenir la chaîne de caractères compressées.

Il faut ensuite initialiser le décompte. Pour cela on crée deux variables :

* `caractere_repete` contiendra le caractère que la fonction est en train de compter. On l'initialise avec le premier caractère du texte.
* `nb_repetitions` contiendra le nombre de caractères identiques consécutifs.

On débute ensuite un parcours de l'ensemble du texte. Pour chaque `caractere`, on se demande s'il est égal à `caractere_repete` :

* si oui, on incrémente `nb_repetitions`
* si non, on ajoute un nouveau couple au résultat (`chaine_compressee += str(nb_repetitions) + caractere_repete + ','`) et on réinitialise les variables `caractere_repete` et `nb_repetitions`

En fin de parcours, le couple correspondant au dernier caractère n'a pas été ajouté : on l'ajoute hors de la boucle. On prend soin de ne pas placer de virgule.

En dernier lieu on renvoie la chaîne compressée.

*Remarque :* la concaténation de chaînes de caractères `chaine += nouveau` peut être coûteuse en Python (du fait de la création d'une nouvelle chaîne de caractères). On peut utiliser la méthode `str.join` qui permet d'éviter ce désagrément.

Il faut alors créer une liste de couples à regrouper dans la chaîne compressée et de les "joindre" en plaçant des virgules entre eux :

```python
def compression_RLE(texte):
    couples = []

    caractere_repete = texte[0]
    nb_repetitions = 0

    for caractere in texte:
        if caractere == caractere_repete:
            nb_repetitions += 1
        else:
            couples.append(str(nb_repetitions) + caractere_repete)
            caractere_repete = caractere
            nb_repetitions = 1

    couples.append(str(nb_repetitions) + caractere_repete)

    return ",".join(couples)
```