# Tests
assert     parenthesage_correct('([()[]]{()})')
assert not parenthesage_correct('{}[(])')
assert not parenthesage_correct('[][')
assert not parenthesage_correct('[]]')

# Tests supplémentaires
assert     parenthesage_correct('('*10+')'*10)
assert     parenthesage_correct('(['*10+'])'*10)
assert     parenthesage_correct('([{'*10+'}])'*10)
assert not parenthesage_correct('{'*10+'}'*10+')')
assert not parenthesage_correct('{'*10+')'*10)