---
author: BNS2022-24.2, puis Nicolas Revéret
title: Parenthésage correct
tags:
  - pile
  - récursivité
---

# Parenthésage correct

On dispose de chaînes de caractères contenant **uniquement** : 

* des parenthèses ouvrantes `(` et fermantes `)`,
  
* des crochets ouvrants `[` et fermants `]`,

* des accolades ouvrantes `{` et fermantes `}`.

On appellera **délimiteur** une parenthèse, un crochet ou une accolade. Un parenthésage est correct si :

* pour chaque délimiteur le nombre de fermants coïncide avec le nombre d'ouvrants,
* en parcourant la chaîne de gauche à droite, pour chaque délimiteur, le nombre d'ouvrant est à tout moment supérieur ou égal au nombre de fermants,
* les différents délimiteurs ne s'entre-croisent pas : `[(])` est incorrect.

???+ example "Exemples" 

    * `([()[]]{()})` est un parenthésage correct,

    * `{}[(])` est incorrect (entre-croisement),

    * `[][` est incorrect (crochet ouvrant en trop),

    * `[]]` est incorrect (crochet fermant en trop).

On souhaite programmer une fonction `parenthesage_correct` qui :

* prend en paramètre une chaîne chaîne de délimiteurs,
  
* renvoie `True` si la chaîne est bien parenthésée et `False` sinon.

On adopte la démarche suivante utilisant une *Pile* :

* on parcourt les caractères de la chaîne,
* si le caractère lu est un délimiteur ouvrant on l'empile,
* si c'est un délimiteur fermant :
    * si la pile est vide, on renvoie `False`,
    * sinon, si le caractère dépilé n'est pas le délimiteur ouvrant du caractère lu, on renvoie `False`,
* à la fin du parcours, si la pile est vide on renvoie `True`, sinon `False`.

On utilise un dictionnaire pour associer facilement délimiteurs fermants et ouvrants.

La classe `Pile` est fournie ci-dessous. Elle est déjà "chargée" en mémoire et donc utilisable sans import dans la zone de saisie.

??? tip "Classe `Pile` (pour information)"

    ```python
    class Pile:
      """Classe définissant une structure de pile"""

      def __init__(self):
          self.contenu = []

      def est_vide(self):
          """Renvoie le booléen True si la pile est vide, False sinon"""
          return self.contenu == []

      def empiler(self, v):
          """Place l'élément v au sommet de la pile"""
          self.contenu.append(v)

      def depiler(self):
          """Retire et renvoie l’élément placé au sommet de la pile,
          si la pile n’est pas vide"""
          if not self.est_vide():
              return self.contenu.pop()
    ```

???+ example "Exemples"

    ```pycon
    >>> parenthesage_correct('([()[]]{()})')
    True
    >>> parenthesage_correct('{}[(])')
    False
    >>> parenthesage_correct('[][')
    False
    >>> parenthesage_correct('[]]')
    False
    ```

{{ IDE('exo') }}
