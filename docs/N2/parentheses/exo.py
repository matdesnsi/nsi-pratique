#--- HDR ---#
class Pile:
    """Classe définissant une structure de pile"""

    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon"""
        return self.contenu == []

    def empiler(self, v):
        """Place l'élément v au sommet de la pile"""
        self.contenu.append(v)

    def depiler(self):
        """Retire et renvoie l’élément placé au sommet de la pile,
        si la pile n’est pas vide"""
        if not self.est_vide():
            return self.contenu.pop()
#--- HDR ---#


def parenthesage_correct(expression):
    ouvrants = {')': '(',
                ...
                }

    pile = Pile()
    for delimeteur in expression:
        if delimeteur in "([{":
            pile.empiler(...)
        else:
            if ...:
                return ...
            elif pile.depiler() != ...:
                return ...

    return pile....


# Tests
assert     parenthesage_correct('([()[]]{()})')
assert not parenthesage_correct('{}[(])')
assert not parenthesage_correct('[][')
assert not parenthesage_correct('[]]')
