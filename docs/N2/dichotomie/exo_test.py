# Tests
assert dichotomie([1, 2, 3, 4], 2)
assert dichotomie([1, 2, 3, 4], 1)
assert dichotomie([1, 2, 3, 4], 4)
assert not dichotomie([1, 2, 3, 4], 0)
assert not dichotomie([1, 2, 3, 4], 5)
assert dichotomie([1], 1)

# Tests supplémentaires
tableau = list(range(0, 101, 2))
for cible in range(101):
    assert dichotomie(tableau, cible) == (cible % 2 == 0)

tableau = list(range(-100, 0, 2))
for cible in range(-100, 0):
    assert dichotomie(tableau, cible) == (cible % 2 == 0)

tableau = list(range(-12, 10, 3))
for cible in range(-12, 10):
    assert dichotomie(tableau, cible) == (cible % 3 == 0)
