---
author: BNS2022-21.2, puis Nicolas Revéret
title: Dichotomie
tags:
  - récursivité
  - boucle
---

# Dichotomie

Compléter la fonction `dichotomie` :

* prenant en argument un tableau non vide de nombres triés dans l'ordre croissant `nombres` et une valeur `cible`
  
* renvoyant `True` si `cible` est une valeur de `tableau`, `False` dans le cas contraire

!!! example "Exemples"

    ```pycon
    >>> dichotomie([1, 2, 3, 4], 2)
    True
    >>> dichotomie([1, 2, 3, 4], 1)
    True
    >>> dichotomie([1, 2, 3, 4], 4)
    True
    >>> dichotomie([1, 2, 3, 4], 0)
    False
    >>> dichotomie([1, 2, 3, 4], 5)
    False
    >>> dichotomie([1], 1)
    True
    ```

{{ IDE('exo') }}
