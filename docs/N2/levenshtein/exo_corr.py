def distance_levenshtein(chaine1: str, chaine2: str) -> int:
    if chaine1 == "":
        return len(chaine2)
    if chaine2 == "":
        return len(chaine1)
    if chaine1[-1] == chaine2[-1]:
        cout = 0
    else:
        cout = 1

    distance = min(
        distance_levenshtein(chaine1[:-1], chaine2) + 1,
        distance_levenshtein(chaine1, chaine2[:-1]) + 1,
        distance_levenshtein(chaine1[:-1], chaine2[:-1]) + cout
    )
    return distance
