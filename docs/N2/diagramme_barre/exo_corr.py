def ajoute_espaces(mot, longueur):
    return mot + " " * (longueur-len(mot))


def barres(categories, valeurs):
    longueur_max = 0
    for mot in categories:
        if len(mot) > longueur_max:
            longueur_max = len(mot)

    diagramme = ""
    for i in range(len(categories)):
        diagramme += ajoute_espaces(categories[i], longueur_max)
        diagramme += " : "
        diagramme += "#"*valeurs[i]
        diagramme += "\n"

    return diagramme


# Tests
assert ajoute_espaces('Anne', 7) == 'Anne   '
assert ajoute_espaces('Luc', 7) == 'Luc    '
assert ajoute_espaces('nsi', 3) == 'nsi'
assert ajoute_espaces('', 2) == '  '
categories = ['Anne', 'Luc', 'Patrick', 'Sam']
valeurs = [10, 8, 5, 15]
diagramme = """Anne    : ##########
Luc     : ########
Patrick : #####
Sam     : ###############
"""
assert barres(categories, valeurs) == diagramme
categories = ['A', 'B', '', "EEEEE"]
valeurs = [1, 1, 0, 5]
diagramme = """A     : #
B     : #
      : 
EEEEE : #####
"""
assert barres(categories, valeurs) == diagramme
