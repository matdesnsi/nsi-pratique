---
author: Nicolas Revéret
title: Diagramme en barres
tags: 
    - string
    - boucle
state : relecture
---

# Diagramme en barres

On souhaite construire un diagramme en barres à partir de deux listes :

* une liste `categories` contenant des chaînes de caractères,
  
* une liste `valeurs` contenant des nombres entiers positifs ou nuls.

On garantit que les deux listes ont la même longueur.

Si l'on prend par exemple : 

* `categories = ['Anne', 'Luc', 'Patrick', 'Sam']`
* et `valeurs = [10, 8, 5, 15]`,

on souhaite obtenir :

```pycon
>>> print(barres(categories, valeurs))
Anne   : ##########
Luc    : ########
Patrick: #####
Sam    : ###############
```

Comme on peut le voir, afin de construire un diagramme convenable, on doit faire en sorte que toutes les "barres" débutent au même niveau. Il faut donc compléter certaines catégories avec des caractères d'espacement. Par exemple :

```pycon
>>> ajoute_espaces('Anne', 7)
'Anne   '
>>> ajoute_espaces('Luc', 7)
'Luc    '
```

On garantit que, dans les paramètres de la fonction `ajoute_espaces`, la chaîne `mot` est toujours de longueur inférieure ou égale à la `longueur` souhaitée. Ainsi, on ne traitera pas le cas où l'on souhaite allonger jusqu'à la longueur 2 une chaîne de 3 caractères.

Compléter les deux fonctions `ajoute_espace` et `barres` ci-dessous :

{{ py('exo', 0, '# TESTS') }}


!!! example "Exemples"

    ```pycon
    >>> ajoute_espaces('Anne', 7)
    'Anne   '
    >>> ajoute_espaces('Luc', 7)
    'Luc    '
    >>> ajoute_espaces('nsi', 3)
    'nsi'
    >>> ajoute_espaces('', 2)
    '  '
    ```

    ```pycon
    >>> categories = ['Anne', 'Luc', 'Patrick', 'Sam']
    >>> valeurs = [10, 8, 5, 15]
    >>> print(barres(categories, valeurs))
    Anne   : ##########
    Luc    : ########
    Patrick: #####
    Sam    : ###############
    ```

    ```pycon
    >>> categories = ['A', 'B', '', "EEEEE"]
    >>> valeurs = [1, 1, 0, 5]
    >>> print(barres(categories, valeurs))
    A    : #
    B    : #
         : 
    EEEEE: #####
    ```

{{ IDE('exo') }}
