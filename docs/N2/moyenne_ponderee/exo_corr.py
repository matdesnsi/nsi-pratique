def proche(a, b):
    return abs(a - b) < 0.000001

def moyenne(nom, d_resultats):
    if nom in d_resultats:
        notes_et_coeff = d_resultats[nom]
        total_points = 0
        total_coefficients = 0
        for matiere in notes_et_coeff:
            note, coefficient = notes_et_coeff[matiere]
            total_points = total_points + coefficient * note
            total_coefficients = total_coefficients + coefficient
        return round(total_points / total_coefficients, 1)
    else:
        return -1


