Il s'agit en fait d'une recherche de minimum réalisée avec une approche gloutonne.

L'intérêt de l'approche gloutonne est de ne pas avoir à obligatoirement lire l'ensemble des valeurs.

Son défaut est que cette méthode n'assure pas que la balle finira à la position de hauteur minimale à l'échelle de l'ensemble du terrain. Mais dans le cas de la balle de golf, cette approche est satisfaisante (on suppose que la balle ne peut pas ressortir d'un "creux").

# Commentaires

{{ IDE('exo_corr') }}

On commence par créer la liste de déplacements possibles.

On initialise ensuite le couple `(delta_i, delta_j)` à un tuple différent de `(0, 0)` afin de rentrer dans la boucle. Celle-ci continuera jusqu'à ce que le couple soit égal `(0, 0)`.

A chaque itération, on initialise `(delta_i, delta_j)` à `(0, 0)` ce qui correspond au cas où la balle ne roule pas (la variation de hauteur vaut alors `0`).

On parcourt ensuite les `deplacements_possibles` afin de déterminer celui qui entraîne une variation de hauteur maximale.

En fin de boucle, on applique ce déplacement.

La fonction de termine en renvoyant la position actuelle de la balle : `return (i, j)`.