def additionneur(a: int, b: int, c: int) -> tuple[int, int]:
    return a ^ b ^ c, (a & b) | (a & c) | (b & c)


def addition_binaire(n1: list[int], n2: list[int]) -> list[int]:
    pass

assert additionneur(0, 0, 0) == (0, 0)
assert additionneur(1, 0, 0) == (1, 0)
assert additionneur(0, 1, 0) == (1, 0)
assert additionneur(0, 0, 1) == (1, 0)
assert additionneur(1, 1, 0) == (0, 1)
assert additionneur(0, 1, 1) == (0, 1)
assert additionneur(1, 0, 1) == (0, 1)
assert additionneur(1, 1, 1) == (1, 1)

assert addition_binaire([1, 0, 1, 0], [1, 0, 1]) == [1, 1, 1, 1]
assert addition_binaire([1, 0, 1, 0], [1, 0, 1, 0, 1]) == [1, 1, 1, 1, 1]
assert addition_binaire([1, 1, 1], [1]) == [1, 0, 0, 0]
assert addition_binaire([1, 1, 1], [1, 1, 1]) == [1, 1, 1, 0]
