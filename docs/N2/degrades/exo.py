def repartir(a_diviser, nb_parts):
    quotient, reste = divmod(a_diviser, nb_parts)
    repartition = [quotient for _ in range(nb_parts)]
    for i in range(reste):
        repartition[i] += 1
    return repartition

def degrade(couleur_1, couleur_2, nb):
    """A partir d'une couleur_1 de départ, d'une couleur_2 d'arrivée
    et d'un nombre nb de points intermédiaires, la fonction calcule
    et renvoie la liste des couleurs d'un dégradé"""
    r1, v1, b1 = couleur_1
    r2, v2, b2 = ...
    delta_r = repartir(..., ...)
    delta_v = ...
    ...
    l_couleurs = [...]
    for i in range(nb):
        ..., ..., ... = l_couleurs[-1]
        l_couleurs.append((..., ..., ...))
    ...
    return ...


# tests

assert degrade((150, 80, 120), (230, 20, 160), 3) == [(150, 80, 120), (170, 65, 130), (190, 50, 140), (210, 35, 150), (230, 20, 160)], "Échec exemple 1"
assert degrade((10, 100, 220), (102, 200, 178), 4) == [(10, 100, 220), (29, 120, 212), (48, 140, 204), (66, 160, 196), (84, 180, 187), (102, 200, 178)], "Échec exemple 2"
