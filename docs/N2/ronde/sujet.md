---
author: Nicolas Revéret
title: Jeu de la ronde
tags:
    - boucle
status: relecture
---

# Le jeu de la ronde

## Règle du jeu

Soit $n$ un entier strictement positif.

$n$ enfants jouent dans une cour de récréation : ils se placent en rond et choisissent au hasard l’un d’entre eux.

A partir de là, la règle est simple :

* Le premier enfant tape sur l’épaule de son voisin de gauche : celui-ci est éliminé. Au début l’enfant 1 élimine donc l’enfant 2
* On passe à l’enfant restant suivant et l’on recommence : il élimine celui situé à sa gauche. Dans l’exemple, l’enfant 3 élimine le 4
* On continue encore et encore jusqu’à ce qu’il ne reste plus qu’un seul enfant : c’est le gagnant ! 

![Le jeu de la ronde](ronde.gif){ width="40%" }

Dans le cas où $n=13$ (comme sur la figure), le gagnant est l’enfant $11$.

Le but du jeu est simple : où se placer pour gagner ?

## Méthode

Dans cet exercice nous allons utiliser un tableau de booléen nommé `joueurs` dans lequel chaque valeur indique que le joueur concerné est encore en jeu (`True`) ou a déjà été éliminé  (`False`).

!!! caution "Attention"

    Les indices des liste Python étant comptabilisés à partir de `0`, avoir `joueurs[2]` égal à `False` signifie que le joueur n°3 a été éliminé.

Ce tableau sera initialisé avec autant de valeurs `True` qu'il y a de joueurs.

La partie se déroulera ainsi :

* On garde trace de l'indice du joueur qui doit jouer
* On cherche dans le tableau l'indice du prochain joueur encore en jeu
* On "élimine" ce joueur
* On cherche l'indice du prochain joueur encore en jeu : ce sera à lui de jouer

Ces différentes étapes continuent jusqu'à ce qu'il ne reste plus qu'un seul joueur encore en jeu.

# Au travail !

Vous devez écrire deux fonctions :

* `indice_prochain_True` prend en argument une liste de booléens ainsi qu'un indice et renvoie l'indice de la prochaine valeur de la liste égale à `True`. On prendra soin de "boucler" les indices : si celui-ci dépasse la longueur de la liste, on le ramène à `0`.
  
    On garantit que la liste `booleens` contient au moins une valeur `True`.

!!! example "Exemples"

    ```pycon
    >>> booleens = [True, True, False, True, False]
    >>> indice_prochain_True(booleens, 0)
    1
    >>> indice_prochain_True(booleens, 1)
    3
    >>> indice_prochain_True(booleens, 3)
    0
    ```

* `gagnant` prend en argument le nombre de joueurs au début de la partie et renvoie la position du joueur gagnant

!!! example "Exemples"

    ```pycon
    >>> gagnant(1)
    1
    >>> gagnant(5)
    3
    >>> gagnant(13)
    11
    ```

{{ IDE('exo') }}