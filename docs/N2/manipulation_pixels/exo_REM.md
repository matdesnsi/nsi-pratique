Liste en compréhension et opérateur ternaire pour des versions ultra compactes, mais néanmoins compréhensibles :

```python
def negatif(image):
    return [[255 - image[i][j] for j in range(largeur(image))] for i in range(hauteur(image))]

def binaire(image, seuil):
    return [[0 if image[i][j] < seuil else 1 for j in range(largeur(image))] for i in range(hauteur(image))]
```
