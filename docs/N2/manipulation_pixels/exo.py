def hauteur(image):
    '''renvoie le nombre de lignes de l'image'''
    return len(image)

def largeur(image):
    '''renvoie la largeur de l'image'''
    return len(image[0])

def negatif(image):
    '''renvoie le négatif de l'image sous la forme d'une liste de listes'''
    # on créé une image de 0 aux mêmes dimensions que le paramètre image
    hauteur_img = ...
    largeur_img = ...
    nouvelle_image = [[0 for k in range(...)] for i in range(...)] 
    for i in range(...):
        for j in range(...):
            nouvelle_image[i][j] = ...
    return ...

def binaire(image, seuil):
    '''renvoie une image binarisée de l'image sous la forme
    d'une liste de listes contenant des 0 si la valeur
    du pixel est strictement inférieure au seuil
    et 1 sinon'''
    # on crée une image de 0 aux mêmes dimensions que le paramètre image
    hauteur_img = ...
    largeur_img = ...
    nouvelle_image = [[0 for j in range(...)] for i in range(...)]
    for i in range(...):
        for j in range(...):
            if ...:
                nouvelle_image[i][j] = ...
            else:
                nouvelle_image[i][j] = ...
    return nouvelle_image    


# tests

image_1 = [[20, 34, 254, 145, 6], [23, 124, 217, 225, 69], [197, 174, 207, 25, 87], [255, 0, 24, 197, 189]]

assert negatif(image_1) == [[235, 221, 1, 110, 249], [232, 131, 38, 30, 186], [58, 81, 48, 230, 168], [0, 255, 231, 58, 66]]
assert binaire(image_1, 120) == [[0, 0, 1, 1, 0], [0, 1, 1, 1, 0], [1, 1, 1, 0, 0], [1, 0, 0, 1, 1]]
