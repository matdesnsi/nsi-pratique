---
title: Nombre de Catalan récursif
author: Vincent-Xavier Jumel
---
# Nombres de Catalan récursif

On définit les nombres de Catalan (nommés en l'honneur de  Eugène Charles
    Catalan (1814-1894)) de plusieurs façons. Par exemple, on peut
considérer qu'il s'agit du nombre d'arbre binaires entiers à $n+1$ feuilles.

Écrire un programme **récursif** `catalan_recursif` qui calcule ce nombre.

!!! example "Exemples"

    ```pycon
    >>> catalan_recursif(0)
    1
    >>> catalan_recursif(1)
    1
    >>> catalan_recursif(2)
    2
    >>> catalan_recursif(3)
    5
    >>> catalan_recursif(4)
    14
    >>> catalan_recursif(5)
    42
    ```

{{ IDE('exo') }}
