def disque_est_dans_rectangle(x, y, r, x_min, y_min, x_max, y_max):
    if x + r >= x_max:
        return False
    if x - r <= x_min:
        return False
    if y + r >= y_max:
        return False
    if y - r <= y_min:
        return False
    return True


def nb_disques_dans_rectangle(rectangle, disques):
    p_A, p_B = rectangle
    x_min, y_min = p_A
    x_max, y_max = p_B
    resultat = 0
    for x, y, r in disques:
        if disque_est_dans_rectangle(x, y, r, x_min, y_min, x_max, y_max):
            resultat += 1
    return resultat


# tests

assert disque_est_dans_rectangle(
    x=30, y=53, r=1,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"

assert not disque_est_dans_rectangle(
    x=30, y=53, r=10,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"

assert not disque_est_dans_rectangle(
    x=0, y=53, r=1,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"


assert nb_disques_dans_rectangle(((10, 50), (80, 60)), []) == 0

assert nb_disques_dans_rectangle(((10, 50), (80, 60)),
     [(30, 53, 1), (30, 53, 10), (0, 53, 1)]) == 1
