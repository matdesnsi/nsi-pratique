---
author: Romain Janvier, Mireille Coilhac, Nicolas Revéret, Franck Chambon
title: Chiffre de César
tags:
  - 1-boucle
  - 2-string
---

# Le chiffre de César

Le chiffrement de César transforme un message en changeant chaque lettre par une autre obtenue par décalage circulaire dans l'alphabet de la lettre d'origine. Par exemple, avec un décalage de 3, le `'A'` se transforme en `'D'`, le `'B'` en `'E'`, ..., le `'X'` en `'A'`, le `'Y'` en `'B'` et le `'Z'` en `'C'`.

Les autres caractères (`'!'`, `'?'`...) ne sont pas transformés et sont simplement recopiés tels quels dans le message codé. 

Dans cet exercice, nous ne prendrons que des lettres majuscules.

On fournit les deux fonctions 

- `indice_de_lettre` : renvoie l'indice dans l'alphabet d'une lettre majuscule.
- `lettre_par_indice` : renvoie la lettre majuscule d'indice donné.


!!! example "Exemples"
    ```pycon
    >>> indice_de_lettre('C')
    2
    >>> lettre_par_indice(4)
    'E'
    ```

!!! tip "Remarque"
    Pour opérer un décalage circulaire d'un indice, on utilise l'opération modulo $26$ qui renvoie un résultat de $0$ inclus à $26$ exclu.

    Décalage de 8 pour la lettre `'Z'`
    ```pycon
    >>> indice_de_lettre('Z')
    25
    >>> 25 + 8
    33
    >>> 33 % 26
    7
    >>> lettre_par_indice(7)
    'H'
    ```
    Le chiffrement de `'z'` sera ici `'H'`.

!!! abstract "Objectif"
    Écrire la fonction `cesar` qui prend en paramètres une chaine de caractères `message` et un nombre entier `decalage` et renvoie le nouveau message chiffré avec le chiffre de César utilisant ce `decalage`.

    On constate que pour déchiffrer un message, il suffit d'utiliser la clé opposée à celle du chiffrement.

!!! example "Exemples"
    ```pycon
    >>> cesar("HELLO WORLD!", 5) 
    'MJQQT BTWQI!'
    >>> cesar("MJQQT BTWQI!", -5) 
    'HELLO WORLD!'
    ```
    
{{ IDE('exo') }}

