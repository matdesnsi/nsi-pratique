def indice_de_lettre(caractere):
    return ord(caractere) - ord('A')
    
def lettre_par_indice(i):
    return chr(ord('A') + i)

def cesar(message, decalage):
    resultat = ''
    for caractere in ...:
        if 'A' <= caractere <= ...:
            i = ...(caractere)
            i = (i + ...) ...
            resultat += ...(i)
        else:
            resultat += ...
    return ...


# tests

assert cesar('HELLO WORLD!', 5) == 'MJQQT BTWQI!'
assert cesar('MJQQT BTWQI!', -5) == 'HELLO WORLD!'

assert cesar('BONJOUR LE MONDE !', 23) == 'YLKGLRO IB JLKAB !'
assert cesar('YLKGLRO IB JLKAB !', -23) == 'BONJOUR LE MONDE !'
