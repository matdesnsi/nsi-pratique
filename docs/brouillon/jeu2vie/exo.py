def encadre(motif):
    "Renvoie le même motif, mais entouré de cellules mortes"
    h, l = len(motif), len(motif[0])
    resultat = []
    resultat.append([0] * (l+2))
    for ligne in motif:
        resultat.append([0] + ligne + [0])
    resultat.append([0] * (l+2))
    return resultat

def decale(motif, di, dj):
    "Renvoie une copie décalée du motif, quitte à perdre de l'information"
    h, l = len(motif), len(motif[0])
    resultat = [[0]*l for i in range(h)]
    for i in range(h - di):
        for j in range(l - dj):
            resultat[i + di][j + dj] = motif[i][j]
    return resultat

def correspond(motif_0, motif_1):
    if len(motif_0) != len(motif_1):
        return False
    else:
        h = len(motif_0)
    if len(motif_0[0]) != len(motif_1[0]):
        return False
    else:
        l = len(motif_0[0])
    for i in range(h):
        for j in range(l):
            if motif_0[i][j] != motif_1[i][j]:
                return False
    return True

def successeur(motif):
    ...

def est_stable(motif):
    ...

def est_periodique(motif):
    ...

def est_vaisseau(motif):
    ...


# tests
MOTIF_A0 = [
    [0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0],
]

MOTIF_A1 = [
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
]

assert correspond(successeur(MOTIF_A0), MOTIF_A1)


MOTIF_B0 = [
    [0, 0, 0, 0, 0],
    [0, 1, 1, 1, 0],
    [0, 1, 1, 0, 0],
    [0, 0, 0, 0, 0],
]

MOTIF_B1 = [
    [0, 0, 1, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0],
]
MOTIF_B2 = [
    [0, 0, 1, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0],
]

assert correspond(successeur(MOTIF_B0), MOTIF_B1)
assert correspond(successeur(MOTIF_B1), MOTIF_B2)



CARRE_STABLE = [
    [0, 0, 0, 0],
    [0, 1, 1, 0],
    [0, 1, 1, 0],
    [0, 0, 0, 0],
]

assert correspond(encadre(CARRE_STABLE), successeur(CARRE_STABLE))


CLIGNOTANT_0 = [
    [0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
]

CLIGNOTANT_1 = [
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 1, 1, 1, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
]

assert correspond(encadre(CLIGNOTANT_0), successeur(CLIGNOTANT_1))
assert correspond(encadre(CLIGNOTANT_1), successeur(CLIGNOTANT_0))

