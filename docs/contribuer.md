# Contribuer

Nous vous invitons à fournir d'autres sujets. Ceux-ci seront relus, commentés, critiqués, améliorés, testés et validés par la communauté sur le [forum MOOC NSI, section "faire communauté"](https://mooc-forums.inria.fr/moocnsi/c/faire-communautc3a9/187).

## Participer à l'élaboration des sujets

Tout se passe sur le forum dans le fil de discussion lié à l'exercice. N'hésitez pas à tester l'exercice chez vous, à demander des précisions à l'auteur, à lui proposer de reformuler telle ou telle question, ...

Si vous testez l'exercice, n'hésitez pas à signaler que les tests proposés sont incomplets, à en donner d'autres.

Pour visualiser le site chez vous, on peut procéder de la façon suivante,
     dans un répertoire de travail dédié :

=== "En téléchargeant"
    [Télécharger](https://gitlab.com/e-nsi/nsi-pratique/-/archive/main/nsi-pratique-main.zip) l'archive au format `zip` et la décompresser.
=== "Avec git"
    ```sh
    git clone https://gitlab.com/e-nsi/nsi-pratique.git
    ```

Activer l'environnement virtuel de travail
=== "Windows"
    ```sh
    python -m venv .venv
    ./.venv/Scripts/Activate.ps1
    pip install -r requirements.txt
    mkdocs serve
    ```
=== "GNU/Linux ou Mac"
    ```sh
    python -m venv .venv
    source .venv/bin/Activate.ps1
    pip install -r requirements.txt
    mkdocs serve
    ```

Lorsque vous avez fini : `deactivate` pour quitter l'environnement virtuel.

## Proposer d'autres sujets d'exercices

Il suffit de préparer

- un fichier `sujet.md` (format Markdown),
- un fichier `exo.py` à compléter,
- un fichier `exo_corr.py` qui donne une solution attendue,
- un fichier `exo_test.py` avec des tests unitaires,
- éventuellement, un fichier `exo_REM.md` (en Markdown) avec des commentaires à ajouter à la solution.

Sur le [forum MOOC NSI, section "faire communauté"](https://mooc-forums.inria.fr/moocnsi/c/faire-communautc3a9/187), postez un message avec les fichiers et l'intitulé du sujet. Si vous êtes habitués de Git, n'hésitez pas à fournir un identifiant [GitLab.com](https://about.gitlab.com) afin que vous puissiez déposer vos sujets.



### Structure du fichier sujet.md

````markdown
author: <Prénom> <Nom>
title: <un titre court>
tags:
  - <2-4 tags>
---

# Un titre qui peut être plus long

<description>

> On n'utilisera pas <truc_interdit_ici>

!!! example "Exemples"

    ```pycon
    >>> ma_fonction(mes_parametres_1)
    resultat_1
    >>> correspond_bien(ma_fonction(mes_parametres_2), resultat_2)
    True
    ```
````

### Structure de exo.py

```python
def ma_fonction(mes_parametres):
    ...


# tests

assert ma_fonction(mes_parametres_1) == resultat_1, "message personnalisé _1"
assert correspond_bien(ma_fonction(mes_parametres_2), resultat_2), "autre message"
```

Il faut inclure ici des tests simples, les mêmes qui sont dans le sujet, mais écrits sous forme d'assertions.

### Structure de exo_test.py

```python
# tests
assert ma_fonction(mes_parametres_1) == resultat_1, "message personnalisé _1"
assert correspond_bien(ma_fonction(mes_parametres_2), resultat_2), "autre message"

# autres tests

assert ma_fonction(mes_parametres_3) == resultat_3, "message personnalisé _3"
assert correspond_bien(ma_fonction(mes_parametres_4), resultat_4), "autre message"
```

- On reprend les tests de l'énoncé sous forme d'assertions.
- On ajoute **beaucoup** de tests !

### Structure de exo_corr.py

Une seule solution **avec** les tests simples.

### Structure de exo_REM.md

Ce fichier peut contenir des remarques sur les difficultés éventuelles de l'exercice ou proposer des corrections alternatives.
