def tri_3_valeurs(nombres):
    ...

# Tests
nombres = [2, 0, 2, 1, 2, 1]
tri_3_valeurs(nombres)
assert nombres == [0, 1, 1, 2, 2, 2]

nombres = [1, 1, 0, 1]
tri_3_valeurs(nombres)
assert nombres == [0, 1, 1, 1]
