---
author: Mireille Coilhac
title: Nombre de répétitions
tags:
  - boucle
--- 

Écrire une fonction python appelée `nb_repetitions` qui prend en paramètres un élément `element` et un tableau de `valeurs` et renvoie le nombre de fois où l’élément `element` apparaît dans le tableau `valeurs`.

> Il est **interdit** d'utiliser la méthode `count`

!!! example "Exemples"

    ```python
    >>> nb_repetitions(5, [2, 5, 3, 5, 6, 9, 5])
    3
    >>> nb_repetitions('A', ['B', 'A', 'B', 'A', 'R'])
    2
    >>> nb_repetitions(12, [1, 7, 21, 36, 44])
    0
    ```

{{ IDE('exo') }}
