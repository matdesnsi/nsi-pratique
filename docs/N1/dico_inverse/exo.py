def inverser(dico):
    ...



assert inverser({'a': 5, 'b': 7}) == {5: 'a', 7: 'b'}, "exemple 1"
assert inverser({'a': 5, 'b': 7, 'c': 5}) == {}, "exemple 2"
assert inverser({"Paris": "Tour Eiffel", "Rome": "Colisée", "Berlin": "Reichtag", "Londres": "Big Ben"}) == \
                {'Tour Eiffel': 'Paris', 'Colisée': 'Rome', 'Reichtag': 'Berlin', 'Big Ben': 'Londres'}, "exemple 3"
assert inverser({"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"}) == {}, "exemple 4"
