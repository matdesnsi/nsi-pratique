---
author: Jean Diraison
title: Dictionnaire inversé
tags:
  - dictionnaire
  - boucle
---
# Dictionnaire inversé

Un dictionnaire associe des valeurs à des clés, comme par exemple `{"Paris": "Tour Eiffel", "Rome": "Colisée", "Berlin": "Reichtag", "Londres": "Big Ben"}` qui associe `"Tour Eiffel"` à la clé `"Paris"`.

Dans certains cas, il est possible d'inverser ce dictionnaire en associant à chaque valeur sa clé d'origine. C'est le cas de l'exemple précédent avec `{"Tour Eiffel": "Paris", "Colisée": "Rome", "Reichtag": "Berlin", "Big Ben": "Londres"}`, mais pas du dictionnaire `{"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"}` puisque la valeur `"L"` est associée à la fois à la clé `"Lyon"` et à la clé `"Lille"`.

Vous devez écrire une fonction `inverser` de paramètre `dico` qui renvoie le dictionnaire inversé de `dico` lorsqu'il peut être construit et le dictionnaire vide `{}` dans le cas contraire.


!!! Exemples

    ```pycon
    >>> inverser({'a': 5, 'b': 7})
    {5: 'a', 7: 'b'}
    >>> inverser({'a': 5, 'b': 7, 'c': 5})
    {}
    >>> inverser({"Paris": "Tour Eiffel", "Rome": "Colisée", "Berlin": "Reichtag", "Londres": "Big Ben"})
    {'Tour Eiffel': 'Paris', 'Colisée': 'Rome', 'Reichtag': 'Berlin', 'Big Ben': 'Londres'}
    >>> inverser({"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"})
    {}
    ```

{{ IDE('exo') }}
