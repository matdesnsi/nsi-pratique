# Commentaires

L'inversion d'un dictionnaire n'est possible que si les valeurs sont toutes distinctes. Ainsi, au cours de la construction du dictionnaire inversé, il suffit de vérifier pour chaque couple `(cle, valeur)` pris dans `dico`, si `valeur` est déjà utilisée comme clé pour en déduire qu'il n'est pas possible d'inverser le dictionnaire.
