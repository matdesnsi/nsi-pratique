---
author: BNS2022-11.2, puis Romain Janvier et Mireille Coilhac
title: Code de César
tags:
  - boucle
  - string
---

# Le code de César

Le codage de César transforme un message en changeant chaque lettre par une autre obtenue par décalage dans l'alphabet de la lettre d'origine. Par exemple, avec un décalage de 3, le `'A'` se transforme en `'D'`, le `'B'` en `'E'`, ..., le `'X'` en `'A'`, le `'Y'` en `'B'` et le `'Z'` en `'C'`. Les autres caractères (`'!'`, `'?'`...) ne sont pas codés et sont simplement recopiés tels quels dans le message codé. Dans cet exercice nous ne prendrons que des décalages positifs.

Ecrire la fonction `cesar` qui prend en paramètres une chaîne de caractères `message` et un nombre entier positif `decalage` et renvoie le nouveau message codé avec le codage de César utilisant ce `decalage`.

!!! example "Exemples"

 
    ```pycon
    >>> cesar('BONJOUR A TOUS. VIVE LA MATIERE NSI !', 4)
    'FSRNSYV E XSYW. ZMZI PE QEXMIVI RWM !'
    >>> cesar('GTSOTZW F YTZX. ANAJ QF RFYNJWJ SXN !', 21)
    'BONJOUR A TOUS. VIVE LA MATIERE NSI !'
    ```  

👉 **Rappel**:  
On rappelle que pour un entier `n` positif, `n % 26` renvoie le reste dans la division de `n` par 26.  
Voici quelques exemples :  

    ```pycon
    >>> 10 % 26
    10
    >>> 26 % 26
    0
    >>> 27 % 26
    1
    >>> 28 % 26
    2
    >>> 53 % 26
    1
    >>> 
    ```  

👉 **Indications**:   
* Vous pourrez utiliser la méthode `.index(caractere)` qui renvoie le rang de `caractere` dans une chaîne de caractères.  

!!! example "Exemples"

 
    ```pycon
    >>> mot = 'BONJOUR'
    >>> mot.index('B')
    0
    >>> mot.index('J')
    3
    >>> 
    ```  

* On donne également 
```pycon
ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
```


{{ IDE('exo') }}

