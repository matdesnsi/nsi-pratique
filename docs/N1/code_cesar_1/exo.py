ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def cesar(message, decalage):
    resultat = ''
    for ... in message:
        if ... in ALPHABET:
            indice = (...) % 26
            resultat = ...
        else:
            resultat = ...
    return resultat

# tests

assert cesar('BONJOUR A TOUS. VIVE LA MATIERE NSI !', 4) == 'FSRNSYV E XSYW. ZMZI PE QEXMIVI RWM !'
assert cesar('GTSOTZW F YTZX. ANAJ QF RFYNJWJ SXN !', 21) == 'BONJOUR A TOUS. VIVE LA MATIERE NSI !'

