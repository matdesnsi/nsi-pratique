def palmares(notes):
    meilleure_note = -1
    eleves_meilleure_note = []
    for eleve in notes:
        if notes[eleve] > meilleure_note:
            meilleure_note = notes[eleve]
            eleves_meilleure_note = [eleve]
        elif notes[eleve] == meilleure_note:
            eleves_meilleure_note.append(eleve)

    return (meilleure_note, eleves_meilleure_note)


# Tests
notes = {'Alice': 18,
         'Bob': 10,
         'Charles': 8,
         'David': 12,
         'Erwann': 17,
         'Fanny': 14,
         'Guillaume': 16,
         'Hugo': 14}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (18, ['Alice'])

notes = {'Bob': 10,
         'Charles': 8,
         'David': 12,
         'Hugo': 14,
         'Fanny': 14}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (14, ['Fanny', 'Hugo'])

notes = {'Romuald': 15,
         'Ronan': 14,
         'David': 15}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (15, ['David', 'Romuald'])
