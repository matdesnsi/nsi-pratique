---
author: Mireille Coilhac et Nicolas Revéret
title: Palmarès
tags:
  - dictionnaire
  - boucle
---

# Liste des élèves ayant obtenus la meilleure note

> :warning: On interdit ici d'utiliser `max`.

Après un devoir, un professeur désire connaître les prénoms de l'élève ou des élèves ayant obtenu la meilleure note.

Les notes sont fournies dans un dictionnaire Python dans lequel les clés sont les prénoms des élèves et les valeurs leur note.

```python
notes = {'Alice': 18,
         'Bob': 10,
         'Charles': 8,
         'David': 12,
         'Erwann': 18,
         'Fanny': 14,
         'Guillaume': 18,
         'Hugo': 14}
```

On garantit que ce dictionnaire n'est pas vide et que toutes les notes sont positives ou nulles.

On désire créer une fonction `palmares` qui prend en paramètre un tel dictionnaire `notes` et renvoie un tuple de deux éléments :

* la note maximale qui a été attribuée

* et la liste contenant les noms du ou des élèves ayant obtenu cette note.

!!! tip "Remarque"

    Il est inutile de se soucier de l'ordre des prénoms dans la liste des résultats. Seule leur présence au sein de la liste importe.

!!! example "Exemples"

    ```pycon
    >>> notes = {'Alice': 18,
    ...          'Bob': 10,
    ...          'Charles': 8,
    ...          'David': 12,
    ...          'Erwann': 17,
    ...          'Fanny': 14,
    ...          'Guillaume': 16,
    ...          'Hugo': 14}
    >>> palmares(notes)
    (18, ['Alice'])
    ```

    ```pycon
    >>> notes = {'Bob': 10,
    ...          'Charles': 8,
    ...          'David': 12,
    ...          'Hugo': 14,
    ...          'Fanny': 14}
    >>> palmares(notes)
    (14, ['Hugo', 'Fanny'])
    >>> # on accepte aussi (14, ['Fanny', 'Hugo'])
    ```

    ```pycon
    >>> notes = {'Romuald': 15,
    ...          'Ronan': 14,
    ...          'David': 15}
    >>> palmares(notes)
    (15, ['David', 'Romuald'])
    >>> # on accepte aussi (15, ['Romuald', 'David'])
    ```

{{ IDE('exo') }}