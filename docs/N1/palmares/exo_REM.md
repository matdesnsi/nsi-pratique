D'un point de vue formel, il s'agit de rechercher la valeur maximale dans un dictionnaire ainsi que la liste des clés associées à cette valeur maximale.

Les notes étant garanties positives ou nulles, on peut initialiser la `meilleure_note` à `-1`.

On parcourt ensuite l'ensemble des clés du dictionnaire (les élèves) en testant les valeurs associées (leurs notes respectives). Si celle-ci est strictement supérieure à la note maximale, on met celle-ci à jour. Si la note de l'élève est égale à la note maximale, on ajoute son prénom à la liste des meilleurs résultats.

On aurait aussi pu procéder en deux étapes : 

* la détermination de la note maximale,
  
* suivie de la sélection des prénoms des élèves ayant obtenu cette note.

```python
def palmares(notes):
    meilleure_note = 0
    for eleve in notes:
        if notes[eleve] > meilleure_note:
            meilleure_note = notes[eleve]

    return (meilleure_note, [eleve for eleve in notes if notes[eleve] == meilleure_note])
```
