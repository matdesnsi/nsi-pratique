# Tests supplémentaires
from random import randint, sample

notes = {'Alice': 18,
         'Bob': 18,
         'Charles': 18,
         'David': 18,
         'Erwann': 17,
         'Fanny': 18,
         'Guillaume': 18,
         'Hugo': 18}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (18, ['Alice',
                                           'Bob',
                                           'Charles',
                                           'David',
                                           'Fanny',
                                           'Guillaume',
                                           'Hugo'])

notes = {'Alice': 20,
         'Bob': 0,
         'Charles': 0,
         'David': 0,
         'Erwann': 20,
         'Fanny': 0,
         'Guillaume': 0,
         'Hugo': 0}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (20, ['Alice',
                                           'Erwann'])

notes = {'Nicolas': 0}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (0, ['Nicolas'])

notes = {'Nicolas': 20}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (20, ['Nicolas'])

alphabet = [chr(k) for k in range(65, 91)]
for _ in range(20):
    eleves = sample(alphabet, 20)
    notes = {eleve: randint(0, 20) for eleve in eleves}
    maxi = max(notes.values())
    note_max, eleves = palmares(notes)
    assert (note_max, sorted(eleves)) == (
        maxi, sorted([e for e in notes if notes[e] == maxi]))
