def ajoute_bord(tab):
    pass


tab1 = [
    [0, 1, 1, 1, 0, 1],
    [0, 0, 1, 0, 0, 1],
    [1, 0, 0, 1, 1, 0],
    [1, 0, 0, 1, 1, 1]
    ]

ajoute_bord(tab1)

tab1_attendu = [
    [0, 1, 1, 1, 0, 1, 4],
    [0, 0, 1, 0, 0, 1, 2],
    [1, 0, 0, 1, 1, 0, 3],
    [1, 0, 0, 1, 1, 1, 4],
]

assert tab1 == tab1_attendu
