---
author: Guillaume CONNAN
title: Max Mat

---

# Ajout d'une colonne contenant le nombre de 1 par ligne

On dispose d'un tableau non vide ne contenant que des `0` et des `1` et on
voudrait lui rajouter une colonne à droite ou figurent les totaux par ligne.

Par exemple, le tableau :

$$
\begin{array}{*{6}{|c|}}
\hline
0&1&1&1&0&1\\
\hline
0&0&1&0&0&1\\
\hline
1&0&0&1&1&0\\
\hline
1&0&0&1&1&1\\
\hline
\end{array}
$$


sera représenté sur machine par la liste de listes suivante : 


```pycon
>>> tab1 = [
...     [0, 1, 1, 1, 0, 1],
...     [0, 0, 1, 0, 0, 1],
...     [1, 0, 0, 1, 1, 0],
...     [1, 0, 0, 1, 1, 1],
... ]
```

et deviendra :

$$
\begin{array}{*{7}{|c|}}
\hline
0&1&1&1&0&1&4\\
\hline
0&0&1&0&0&1&2\\
\hline
1&0&0&1&1&0&3\\
\hline
1&0&0&1&1&1&4\\
\hline
\end{array}
$$


Vous coderez une fonction `ajoute_bord(tab)` qui modifie le paramètre `tab` en
lui ajoutant la colonne décrite précédemment :

!!! example "Exemples"

    ```pycon
    >>> tab1 = [
    ...     [0, 1, 1, 1, 0, 1],
    ...     [0, 0, 1, 0, 0, 1],
    ...     [1, 0, 0, 1, 1, 0],
    ...     [1, 0, 0, 1, 1, 1],
    ... ]
    >>> ajoute_bord(tab1)
    >>> tab2 = [
    ...     [0, 1, 1, 1, 0, 1, 4],
    ...     [0, 0, 1, 0, 0, 1, 2],
    ...     [1, 0, 0, 1, 1, 0, 3],
    ...     [1, 0, 0, 1, 1, 1, 4],
    ... ]
    >>> tab1 == tab2
    True
    ```

{{ IDE('exo') }}
