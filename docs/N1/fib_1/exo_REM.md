# Commentaires

## Version récursive

```python
def fibonacci(n):
    if n < 2:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)
```

Cette version est très simple à écrire, mais devient très lente à partir de `n = 25` environ. Les mêmes calculs sont demandés de très nombreuses fois.

## Version itérative

```python
def fibonacci(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a+b
    return a
```

Il s'agit de programmation dynamique. On part de $a, b = 0, 1$, les deux premiers termes consécutifs, et on progresse de $n$ étapes.

À chaque étape,

- Le nouveau $a$ devient le terme suivant $a$ : l'ancien $b$.
- Le nouveau $b$ devient le terme suivant $b$ : la somme de $a$ et de $b$.

À la fin, le terme d'indice $n$ est $a$, $b$ est le suivant.


## Version avec une liste pour mémoïser

```python
fibonacci_mem = [0, 1]

def fibonacci(n):
    i = len(fibonacci_mem)
    while i <= n:
        fib_i = fibonacci_mem[i - 1] + fibonacci_mem[i - 2]
        fibonacci_mem.append(fib_i)
        i += 1
    return fibonacci_mem[n]
```

Cette fonction complète si nécessaire une liste `fibonacci_mem`.

- `fibonacci_mem[n]` stocke le résultat de `fibonacci(n)`
- Tant que la longueur de cette liste est insuffisante, on ajoute un nouveau terme.
- `fib_i` sera le terme d'indice `i`
- `i` est la taille de la liste, qui augmente de 1 à chaque tour de boucle.

## Version mathématique

On peut démontrer que pour $n$ petit, il suffit de prendre l'arrondi de $\dfrac{\phi^n}{\sqrt 5}$, où $\phi = \dfrac{1+\sqrt 5}2$.

```python
from math import sqrt
PHI = (1 + sqrt(5)) / 2

def fibonacci(n):
    return round(PHI**n / sqrt(5))
```

Cette version devient fausse dès que le résultat entier dépasse la capacité de la mantisse du conteneur flottant.
