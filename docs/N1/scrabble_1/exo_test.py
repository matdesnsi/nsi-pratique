# tests
assert calcul_score("KAYAK") == 32
assert calcul_score("INFORMATIQUE") == 23
assert calcul_score("") == 0

# autres tests
assert calcul_score("K") == 10
assert calcul_score("GIRAFE") == 10
assert calcul_score("LAPSUS") == 8


