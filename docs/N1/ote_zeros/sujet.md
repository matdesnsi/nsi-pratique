---
author: Guillaume CONNAN
title: Ôte zéros

---

# Ôte zéros

On dispose  d'une liste de `0`  et de `1`  qui représente un nombre  entier en
base 2, le bit de poids faible se trouvant à gauche. 

On désire créer une fonction `ote_zeros(bins)`
qui  renvoie la  liste obtenue  en enlevant  les éventuels  zéros se  trouvant à
droite de `bins`.

Si `bins` est  une liste  ne contenant  que des `0`, c'est  qu'elle représente
l'entier zéro et la liste renvoyée sera `[0]`.

Un nombre entier ne pouvant pas être  représenté par une liste vide, on veillera
à utiliser `assert` afin de parer à cette éventualité.


!!! example "Exemples"

    ```pycon
    >>> bins1 = [1, 0, 1, 0, 0, 0, 0]
    >>> ote_zeros(bins1)
    [1, 0, 1]
    >>> bins2 = [0, 0, 0, 0]
    >>> ote_zeros(bins2)
    [0]
    >>> xs3 = [1]
    >>> ote_zeros(bins3)
    [1]
    >>> xs4 = []
    >>> ote_zeros(bins4)
    AssertionError: La liste ne peut être vide
    ```

{{ IDE('exo') }}
