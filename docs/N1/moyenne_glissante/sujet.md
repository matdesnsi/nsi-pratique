---
author: Vincent-Xavier Jumel
title: Moyenne glissante

---

Dans le cadre d'une série de mesure, on a besoin d'obtenir la moyenne sur
les 7 dernières occurences d'une série statistique et non sur la totalité de
la série.

Écrire une fonction `moyenne_glissante` qui renvoie les moyennes sur les 7
dernières occurences de la série. Si la liste passée en argument est de
longueur inférieure à 7, on renvoie `[]`.

> On n'utilisera ni `sum`, ni `mean` ni les tranches (`liste[i:i+7]`)

!!! example "Exemples"

    ```pycon
    >>> moyenne_glissante([1,1,1,1,1,1])
    []
    >>> moyenne_glissante([1,1,1,1,1,1,1])
    [1.0]
    >>> moyenne_glissante([1,2,3,4,5,6,7,8,9,10,11,12]
    [4.714285714285714, 4.0, 5.0, 6.0, 7.0, 8.0]
    ```

{{ IDE('exo') }}
