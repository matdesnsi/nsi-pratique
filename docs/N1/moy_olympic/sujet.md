---
author: Guillaume CONNAN
title: Moyenne olympique

---

# Moyenne olympique

Le championnat d'Europe de la bourrée auvergnate va avoir lieu à Nantes cette
année. Chaque athlète reçoit une note du panel  de `n` juges (il y en a au moins
2).

La note finale est calculée en enlevant les deux notes extrêmes et
en calculant la moyenne des `n - 2` notes restant. 

Vous  devez aider  les  organisateurs à  donner  la note  finale  en créant  une
fonction  `moy_bourree(notes)` où  `notes` est  une liste  contenant les  notes des
juges (les notes sont des nombres entiers ou à virgule flottante entre 0 et 10).


**CONTRAINTE** : on n'utilisera ni `max`, ni `min`, ni `sum`.

!!! example "Exemples"

    ```pycon
	>>> notes1 = [1, 2, 4 , 5]
	>>>  moy_bourree(notes1)
    3.0
	>>> notes2 = [1, 1, 1, 1, 1, 1]
	>>> moy_bourree(notes2)
    1.0
	>>> notes3 = [1]
	>>> moy_bourree(notes3)
    AssertionError: Pas assez de juges
    ```

{{ IDE('exo') }}
