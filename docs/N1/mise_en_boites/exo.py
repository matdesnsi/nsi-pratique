def poids_boite(boite):
    ...


def ranger(poids_objets, poids_max):
    ...


# Tests
assert poids_boite([]) == 0
assert poids_boite([5, 3, 7]) == 15
assert ranger([5, 3, 7], 8) == [[5, 3], [7]]
assert ranger([5, 3, 7], 15) == [[5, 3, 7]]
assert ranger([3, 4, 9, 3], 10) == [[3, 4, 3], [9]]
