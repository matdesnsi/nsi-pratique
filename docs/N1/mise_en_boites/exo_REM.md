Il s'agit du problème classique du *bin-packing*. On applique ici la méthode de la première position : on place chaque objet dans la première boîte pouvant l'accueillir.

# Commentaires

{{ IDE('exo_corr') }}

La fonction `poids_boite` ne présente pas difficultés : on crée une variable `total` initialisée à `0` puis on parcourt les objets et on additionne chaque poids à ce `total`.

Dans la fonction `ranger` :

* on crée la liste des boîtes vides comme le suggère l'énoncé
* on parcourt l'ensemble des objets :
    * pour chacun d'entre eux, on cherche l'indice de la première boîte pouvant l'accueillir
    * on ajoute l'objet à cette boîte
* on ne renvoie que les boîtes non vides (le filtre est fait dans une liste par compréhension)