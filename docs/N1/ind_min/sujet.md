---
author: Mireille Coilhac, Franck Chambon
title: Indice du minimum
tags:
  - boucle
---
# Indice du minimum d'un tableau

Écrire une fonction `indice_min` qui prend en paramètre un tableau **non vide** de nombres et qui renvoie l'indice de la première occurrence du minimum de ce tableau.

> - Les tableaux seront représentés sous forme de liste Python.
> - On n'utilisera pas les fonctions `min` et `index`.

!!! example "Exemples"

    ```pycon
    >>> indice_min([5])
    0
    >>> indice_min([2, 4, 1, 1])
    2
    >>> indice_min([5, 3, 2, 5, 2])
    2
    ```

{{ IDE('exo') }}
