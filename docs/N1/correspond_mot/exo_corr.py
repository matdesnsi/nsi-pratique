def correspond(mot, mot_a_trous):
    if len(mot) != len(mot_a_trous):
        return False
    for i in range(len(mot)):
        if mot_a_trous[i] != '.' and mot_a_trous[i] != mot[i]:
            return False
    return True

# tests

assert correspond("INFORMATIQUE", "INFO.MA.IQUE") == True
assert correspond("AUTOMATIQUE", "INFO.MA.IQUE") == False
assert correspond("INFO", "INFO.MA.IQUE") == False
assert correspond("INFORMATIQUES", "INFO.MA.IQUE") == False

