## Commentaires

### Version simple

{{ IDE('exo_corr') }}


### Version fonctionnelle

```python
def correspond(mot, mot_a_trous):
    return len(mot) == len(mot_a_trous) and all(
            mot_a_trous[i] == mot[i] or mot_a_trous[i] == '.'
                for i in range(len(mot))
        )
```

La fonction `all` fait aussi une évaluation paresseuse et s'interrompt dès que `False` est rencontré.
