def l_max_equilibree(bits):
    cumul = [0]
    for x in bits:
        cumul.append(cumul[-1] + x)
    l_max = 0
    for i_fin in range(1, 1+len(bits)):
        for i_debut in range(i_fin):
            if 2*(cumul[i_fin] - cumul[i_debut]) == i_fin - i_debut:
                if i_fin - i_debut > l_max:
                    l_max = i_fin - i_debut
    return l_max




# tests
assert l_max_equilibree([0, 0, 1, 0, 0, 0, 1, 1, 0, 0]) == 6

assert l_max_equilibree([1, 1, 1, 1]) == 0

assert l_max_equilibree([1, 0, 1, 0, 1]) == 4
