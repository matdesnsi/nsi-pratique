import drawSvg as draw


def ajoute_grille(d, n, m):
    for i in range(n + 1):
        d.append(draw.Lines(
             0*m, 20*i,
            20*m, 20*i,
            close=False,
            stroke_width=2,
            stroke='grey')
        )
        d.append(draw.Circle(0*m,  20*i, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))
        d.append(draw.Circle(20*m, 20*i, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))

    for j in range(m + 1):
        d.append(draw.Lines(
            20*j,  0*n,
            20*j, 20*n,
            close=False,
            stroke_width=2,
            stroke='grey')
        )
        d.append(draw.Circle(20*j,  0*n, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))
        d.append(draw.Circle(20*j, 20*n, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))

def pose_trou(i, j, couleur='black'):
    d.append(draw.Lines((i+0) * large, (j+0) * large,
                        (i+1) * large, (j+0) * large,
                        (i+1) * large, (j+1) * large,
                        (i+0) * large, (j+1) * large,
                        close=True,
                fill=couleur, stroke_width=2,
                stroke='black'))

def pose_triomino(i, j, direction):
    if direction == 1:
        d.append(draw.Lines((i+0) * large, (j+0) * large,
                            (i+2) * large, (j+0) * large,
                            (i+2) * large, (j+1) * large,
                            (i+1) * large, (j+1) * large,
                            (i+1) * large, (j+2) * large,
                            (i+0) * large, (j+2) * large,
                            close=True,
                    fill='red', stroke_width=2,
                    stroke='black'))
    if direction == 2:
        d.append(draw.Lines((i+0) * large, (j+0) * large,
                            (i+0) * large, (j-1) * large,
                            (i+1) * large, (j-1) * large,
                            (i+1) * large, (j+1) * large,
                            (i-1) * large, (j+1) * large,
                            (i-1) * large, (j+0) * large,
                            close=True,
                    fill='yellow', stroke_width=2,
                    stroke='black'))
    if direction == 3:
        d.append(draw.Lines((i+1) * large, (j+0) * large,
                            (i+2) * large, (j+0) * large,
                            (i+2) * large, (j+1) * large,
                            (i+0) * large, (j+1) * large,
                            (i+0) * large, (j-1) * large,
                            (i+1) * large, (j-1) * large,
                            close=True,
                    fill='blue', stroke_width=2,
                    stroke='black'))
    if direction == 0:
        d.append(draw.Lines((i-1) * large, (j+0) * large,
                            (i+1) * large, (j+0) * large,
                            (i+1) * large, (j+2) * large,
                            (i+0) * large, (j+2) * large,
                            (i+0) * large, (j+1) * large,
                            (i-1) * large, (j+1) * large,
                            close=True,
                    fill='green', stroke_width=2,
                    stroke='black'))


n, m = 10, 10
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)

for i in range(8):
    d.append(draw.Text(f'{i}', 16, 5, (n-i-2)*large + 4, fill='grey'))
    d.append(draw.Text(f'{i}', 16, (i+1)*large + 5, (m-1)*large + 4, fill='grey'))

for triomino in [
(6, 4, 2),(7, 2, 0),(8, 4, 2),(5, 1, 1),(8, 1, 0),(4, 5, 3),(3, 6, 3),(2, 7, 3),(1, 8, 3),(4, 8, 2),(1, 5, 1),(6, 6, 2),(7, 7, 2),(5, 8, 3),(8, 8, 2),(8, 5, 0),(3, 3, 1),(2, 2, 1),(1, 4, 3),(1, 1, 1),(4, 1, 0),
]:
    pose_triomino(*triomino)

pose_trou(5, 3)

d.saveSvg(f'indice.svg')
print(f"![](images/indice.svg)")

n, m = 18, 18
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
(6, 12, 2),(7, 10, 0),(8, 12, 2),(5, 9, 1),(8, 9, 0),(4, 13, 3),(3, 14, 3),(2, 15, 3),(1, 16, 3),(4, 16, 2),(1, 13, 1),(6, 14, 2),(7, 15, 2),(5, 16, 3),(8, 16, 2),(8, 13, 0),(3, 11, 1),(2, 10, 1),(1, 12, 3),(1, 9, 1),(4, 9, 0),(9, 8, 0),(10, 10, 2),(11, 11, 2),(9, 12, 3),(12, 12, 2),(12, 9, 0),(13, 13, 2),(11, 14, 3),(10, 15, 3),(9, 16, 3),(12, 16, 2),(9, 13, 1),(14, 14, 2),(15, 15, 2),(13, 16, 3),(16, 16, 2),(16, 13, 0),(14, 11, 0),(15, 10, 0),(16, 12, 2),(13, 9, 1),(16, 9, 0),(7, 7, 1),(6, 6, 1),(5, 8, 3),(5, 5, 1),(8, 5, 0),(4, 4, 1),(3, 6, 3),(2, 7, 3),(1, 8, 3),(4, 8, 2),(1, 5, 1),(3, 3, 1),(2, 2, 1),(1, 4, 3),(1, 1, 1),(4, 1, 0),(6, 3, 0),(7, 2, 0),(8, 4, 2),(5, 1, 1),(8, 1, 0),(10, 7, 0),(11, 6, 0),(12, 8, 2),(9, 5, 1),(12, 5, 0),(13, 4, 0),(14, 6, 2),(15, 7, 2),(13, 8, 3),(16, 8, 2),(16, 5, 0),(11, 3, 1),(10, 2, 1),(9, 4, 3),(9, 1, 1),(12, 1, 0),(14, 3, 0),(15, 2, 0),(16, 4, 2),(13, 1, 1),(16, 1, 0),
]:
    pose_triomino(*triomino)

pose_trou(5, 11)

d.saveSvg(f'indice2.svg')
print(f"![](images/indice2.svg)")


n, m = 6, 16
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
    (2, 3, 0),
    (5, 3, 1),
    (10, 3, 2),
    (13, 3, 3),
]:
    pose_triomino(*triomino)

for i in range(4):
    d.append(draw.Text(f'Sens {i}', 16, (i*4 + 1)*large, 1*large + 4, fill='grey'))

d.saveSvg(f'triominos.svg')
print(f"![](images/triominos.svg)")

n, m = 4, 4
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
    (1, 2, 3),
]:
    pose_triomino(*triomino)
pose_trou(2, 1)

for i in range(2):
    d.append(draw.Text(f'{i}', 16, 5, (n-i-2)*large + 4, fill='grey'))
    d.append(draw.Text(f'{i}', 16, (i+1)*large + 5, (m-1)*large + 4, fill='grey'))

d.saveSvg(f'ex2.svg')
print(f"![](images/ex2.svg)")




n, m = 5, 5
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
    (4, 3, 2),
    (1, 2, 1),
    (3, 1, 0),
    (1, 0, 1),
    (4, 0, 0),
]:
    pose_triomino(*triomino)
pose_trou(2, 3)

for i in range(4):
    d.append(draw.Text(f'{i}', 16, 5, (n-i-2)*large + 4, fill='grey'))
    d.append(draw.Text(f'{i}', 16, (i+1)*large + 5, (m-1)*large + 4, fill='grey'))

d.saveSvg(f'ex4.svg')
print(f"![](images/ex4.svg)")

