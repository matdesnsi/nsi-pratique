# tests
assert est_pavage(2, 1, 1, pavage_triominos(2, 1, 1)) == True

assert est_pavage(4, 0, 1, pavage_triominos(4, 0, 1)) == True

assert est_pavage(8, 5, 4, pavage_triominos(8, 5, 4)) == True

# autres tests
assert pavage_triominos(1, 0, 0) == []

for a in range(16):
    i, j = divmod(a, 4)
    assert est_pavage(4, i, j, pavage_triominos(4, i, j)) == True
    assert est_pavage(8, i, j, pavage_triominos(8, i, j)) == True

for e in range(4, 7):
    n = 2**e
    i, j = 1, 1
    assert est_pavage(n, i, j, pavage_triominos(n, i, j)) == True
    i, j = 2, 3
    assert est_pavage(n, i, j, pavage_triominos(n, i, j)) == True
