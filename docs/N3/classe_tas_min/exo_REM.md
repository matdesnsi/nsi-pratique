## Commentaires

### Version construite

{{ IDE('exo_corr') }}

### Version incluse dans Python

Il est possible d'utiliser le [module Python `heapq`](https://docs.python.org/fr/3/library/heapq.html).

Les fonctions principales suivantes sont fournies :

Création d'un tas
    : utilisez une liste initialisée à [].

`heapq.heappush(heap, item)`
    : Introduit la valeur `item` dans le tas `heap`, en conservant l'invariance du tas.

`heapq.heappop(heap)`
    : Extraie le plus petit élément de `heap` en préservant l'invariant du tas. Si le tas est vide, une exception `#!py IndexError` est levée.


Ce qui donne une version bien plus courte et efficace :

{{ py('exo_var') }}
