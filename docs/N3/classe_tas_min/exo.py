class TasMin:
    def __init__(self):
        self.donnees = ...
        self.effectif = ...
    
    def est_vide(self):
        return self.effectif == ...
    
    def echange(self, i, j):
        "échange les données aux indices i et j"
        temp = self.donnees[i]
        self.donnees[i] = ...
        self.donnees[j] = ...

    def ajoute(self, element):
        self.donnees #...
        self.effectif = ...
        i = self.effectif
        i_parent = ...
        while ...:
            self.echange(i_parent, i)
            i = i_parent
            i_parent = ...

    def extrait_min(self):
        if ...:
            raise IndexError("Tas vide ; impossible d'extraire le minimum")
        
        if self.effectif == 1:
            self.effectif = ...
            #...
            return ...
        
        mini = ... # l'élément minimal à renvoyer à la fin

        # On place à la racine le dernier élément
        self.donnees[...] = self.donnees.pop()
        self.effectif = ...

        # On va le remettre à une place qui respecte la règle
        i = 1
        j = 0
        while i != j:
            j = i
            if 2*i <= self.effectif:
                if self.donnees[...] < self.donnees[...]:
                    j = 2 * i
                    # on peut échanger avec l'enfant gauche
            if ...:
                if ...:
                    j = ...
                    # on peut et il vaut mieux échanger avec l'enfant droite
            if i != j:
                self.echange(i, j)
        
        # fin
        return ...



# tests
test = TasMin()

## test est_vide
assert test.est_vide()

## test un ajoute/extrait
univers = 42
test.ajoute(univers)
element = test.extrait_min()
assert element == univers, "On doit retrouver 42"
assert test.est_vide(), "Le tas doit être vide"

## test plusieurs ajout/extrait
###  dans l'ordre
premiers = [2, 3, 5, 7, 11]
nb_premiers = len(premiers)

for p in premiers:
    test.ajoute(p)
assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
assert test.est_vide()

###  dans l'ordre inverse
premiers = [2, 3, 5, 7, 11]

for p in reversed(premiers):
    test.ajoute(p)
assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
assert test.est_vide()

###  dans le désordre
from random import shuffle
bazar = premiers.copy()
for _ in range(100):
    shuffle(bazar)
    for élément in bazar:
        test.ajoute(élément)
    shuffle(bazar)
    assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
    assert test.est_vide()
