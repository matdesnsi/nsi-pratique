from heapq import heappush, heappop

class TasMin:
    # version avec la bibliothèque

    def __init__(self):
        self.donnees = []
    
    def est_vide(self):
        return self.donnees == []
    
    def ajoute(self, element):
        heappush(self.donnees, element)

    def extrait_min(self):
        mini = heappop(self.donnees)
        return mini


# tests
test = TasMin()

## test est_vide
assert test.est_vide()

## test un ajoute/extrait
univers = 42
test.ajoute(univers)
element = test.extrait_min()
assert element == univers, "On doit retrouver 42"
assert test.est_vide(), "Le tas doit être vide"

## test plusieurs ajout/extrait
###  dans l'ordre
premiers = [2, 3, 5, 7, 11]
nb_premiers = len(premiers)

for p in premiers:
    test.ajoute(p)
assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
assert test.est_vide()

###  dans l'ordre inverse
for p in reversed(premiers):
    test.ajoute(p)
assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
assert test.est_vide()

###  dans le désordre
from random import shuffle
bazar = premiers.copy()
for _ in range(100):
    shuffle(bazar)
    for élément in bazar:
        test.ajoute(élément)
    shuffle(bazar)
    assert [test.extrait_min() for _ in range(nb_premiers)] == premiers
    assert test.est_vide()
