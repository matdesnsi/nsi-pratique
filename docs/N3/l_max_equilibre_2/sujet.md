---
author: Franck Chambon
title: Intervalle équilibré
tags:
  - 9-prog.dynamique
---
# Longueur maximale d'intervalle équilibré

On considère une liste `bits` constituée uniquement de 0 et 1. Écrire une fonction telle que `l_max_equilibree(bits)` renvoie la longueur maximale d'une tranche qui contient autant de 0 que de 1.

!!! example "Exemples"
    ```pycon
    >>> bits = [0, 0, 1, 0, 0, 0, 1, 1, 0, 0]
    >>> l_max_equilibree(bits)
    6
    ```
    En effet, la tranche `[1, 0, 0, 0, 1, 1]` est équilibrée et de longueur maximale.

    ```pycon
    >>> bits = [1, 1, 1, 1]
    >>> l_max_equilibree(bits)
    0
    ```
    En effet, la tranche `[]` est équilibrée et de longueur maximale.

    ```pycon
    >>> bits = [1, 0, 1, 0, 1]
    >>> l_max_equilibree(bits)
    4
    ```
    En effet, la tranche `[1, 0, 1, 0]` est équilibrée et de longueur maximale. Il y a également `[0, 1, 0, 1]` équilibrée et aussi de longueur maximale.


:warning: On attend un algorithme qui fait une simple boucle. Il faudra sauvegarder une information utile à chaque tour de boucle.

{{ IDE('exo', MAX=1000) }}


!!! tip "Indice 1"
    On pourra calculer pour chaque indice la différence `delta` entre le nombre de 1 et le nombre de 0 depuis le début de la liste.

    Lorsqu'on rencontre à nouveau une même valeur de delta, on déduit une tranche équilibrée.

!!! tip "Indice 2"
    On va alors stocker le premier indice où l'on rencontre `delta`. On pourra noter `i_bonus_1[k]` le premier indice où `delta` est égal à `+k`, et `i_bonus_0[k]` le premier indice où `delta` est égal à `-k`.
    
    On pourra utiliser un dictionnaire `i_bonus`, ou deux listes `i_bonus_1` et `i_bonus_0`.

!!! tip "Indice 3"
    On pourra compléter le code

    ```python
    def l_max_equilibree(bits):
        i_bonus_1 = [0]
        i_bonus_0 = [0]
        l_max = 0
        delta = 0
        for i, x in enumerate(bits):
            if x == 1:
                delta = ...
            else:
                delta = ...
            if delta > len(i_bonus_1) - 1:
                i_bonus_1.append(...)
            elif ...:
                i_bonus_0.append(...)
            elif delta >= 0:
                if i - i_bonus_1[delta] > l_max:
                    l_max = ...
            else: # delta <= 0:
                if ...:
                    ...
        return ...
```
