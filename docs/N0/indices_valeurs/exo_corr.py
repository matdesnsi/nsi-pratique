def somme_pairs(nombres):
    # Somme des nombres d'indices pairs
    somme_indice = 0
    for i in range(len(nombres)):
        if i % 2 == 0:
            somme_indice += nombres[i]

    # Somme des nombres pairs
    somme_nombres = 0
    for x in nombres:
        if x % 2 == 0:
            somme_nombres += x


    return (somme_indice, somme_nombres)

# Tests
assert somme_pairs([]) == (0, 0)
assert somme_pairs([4, 6, 3]) == (7, 10)
