# Tests
assert somme_pairs([]) == (0, 0)
assert somme_pairs([4, 6, 3]) == (7, 10)

# Tests supplémentaires
assert somme_pairs([-17, 9, -8, -4, 3, 15, 12, -2, -19, 10]) == (-29, 8)
assert somme_pairs([-5, -2, -20, -5, -18, -12, -2, -20, -10, -10]) == (-55, 94)
