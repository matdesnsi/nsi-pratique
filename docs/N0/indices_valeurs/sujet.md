---
author: Nicolas Revéret
title: Indice ou valeur
tags:
    - 0-simple
    - 1-boucle
status: relecture
---

# Indices ou valeurs ?

On donne un tableau de nombres `nombres`.

Compléter le code dela fonction `somme_pairs` qui :

* prend le tableau en argument,
* renvoie le couple formé de :
  * la somme des nombres placés à des indices pairs 
  * et de la somme des nombres pairs.

Par exemple :

```pycon
>>> # indices  0  1  2
>>> nombres = [4, 6, 3]
>>> somme_pairs(tableau)
(7, 10)
```

En effet, :

* il y a deux indices pairs (`0` et `2`) et la somme des valeurs correspondantes vaut `7`.
* il y a deux nombres pairs (`4` et `6`) et la somme des valeurs correspondantes vaut `10`.

On rappelle qu'il est possible de tester la parité d'un nombre `n` en faisant `n % 2 == 0` :

```pycon
>>> 14 % 2 == 0
True
>>> 15 % 2 == 0
False
```


!!! example "Exemples"

    ```pycon
    >>> somme_pairs([]) 
    (0, 0)
    >>> somme_pairs([4, 6, 3])
    (7, 10)
    ```

{{ IDE('exo') }}
