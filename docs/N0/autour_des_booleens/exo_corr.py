# 3 est-il strictement inférieur à 4 ?
test_1 = 3 < 4

# 3 est-il supérieur ou égal à 4 ?
test_2 = 3 >= 4

# le caractère 'n' est-il dans la chaîne 'bonjour' ?
test_3 = 'n' in 'bonjour'

# Le calcul 3 + 5 * 2 vaut-il 16 ?
test_4 = 3 + 5 * 2 == 16

# 5 est-il un nombre pair ?
test_5 = 5 % 2 == 0

# 12 est-il dans la liste [k for k in range(12)] ?
test_6 = 12 in [k for k in range(12)]

# `ju` n'est-il pas `bonjour` ?
test_7 = 'ju' not in 'bonjour'

# Est-ce que 3 est égal à 1 + 2 et `a` est dans `Boole` ?
test_8 = (3 == 1 + 2) and ('a' in 'Boole')

# Est-ce que 3 est égal à 1 + 2 ou `a` est dans `Boole` ?
test_9 = (3 == 1 + 2) or ('a' in 'Boole')

# 6 est-il un nombre pair et un multiple de 3 ?
test_10 = (6 % 2 == 0) and (6 % 3 == 0)

# 648 est-il dans la table de 2, de 3 et de 4 ?
test_11 = (648 % 2 == 0) and (648 % 3 == 0) and (648 % 4 == 0)

# 25 est-il compris entre 24 et 26 (au sens strict) ?
test_12 = (25 > 24) and (25 < 26)
# ou
test_12 = (24 < 25 < 26)

# 'a' est-il dans `hello` ou dans `hallo` ?
test_13 = ('a' in 'hello') or ('a' in 'hallo')

# 8 est-il égal à 4 * 2 et différent de 9 - 1 ?
test_14 = (8 == 4 * 2) and (8 != 9 - 1)

# 7 est-il compris entre 1 et 5 (au sens large) ou strictement supérieur à 6 ?
test_15 = ((7 >= 1) and (7 <= 5) or (7 >= 6))
