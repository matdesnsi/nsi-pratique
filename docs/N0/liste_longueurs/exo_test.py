# Tests
prenoms = ['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas']
assert liste_longueur(prenoms, 7) == ['Francky', 'Charles', 'Nicolas']
assert liste_longueur(prenoms, 3) == ['Léa']
assert liste_longueur(prenoms, 10) == []

# Tests supplémentaires
