---
author: charles Poulmaire
title: Liste de prénoms
tags:
  - 1-boucle
---

# Liste des longueurs

Compléter la fonction `liste_longueur` afin de renvoyer une liste de prénoms d'une longueur donnée.

!!! example "Exemples"

    ```pycon
	>>> liste_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 7)
    ['Francky', 'Charles', 'Nicolas']
	>>> liste_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 3)
    ['Léa'] 
	>>> liste_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 10)
    [] 
    ```

{{ IDE('exo') }}