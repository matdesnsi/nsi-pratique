def liste_longueur(liste, ...):
    liste_noms = []
    for prenom in liste :
        if ... == longueur :
            liste_noms.append(prenom)
    return liste_noms


# Tests
prenoms = ['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas']
assert liste_longueur(prenoms, 7) == ['Francky', 'Charles', 'Nicolas']
assert liste_longueur(prenoms, 3) == ['Léa']
assert liste_longueur(prenoms, 10) == []