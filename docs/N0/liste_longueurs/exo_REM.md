## Commentaires

{{ IDE('exo_corr') }}

On pourrait aussi utiliser une liste en compréhension:

```pycon
def liste_longueur(liste, longueur):
    return [prenom for prenom in liste if len(prenom) == longueur]
```