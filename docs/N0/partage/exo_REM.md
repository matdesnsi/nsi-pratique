## Commentaires

{{ IDE('exo_corr') }}

### Solution alternative

On pourrait aussi utiliser les listes en compréhension :

```python
def partage(valeurs, effectif_debut):
    taille = len(valeurs)
    return (
        [valeurs[i] for i in range(effectif_debut)],
        [valeurs[i] for i in range(effectif_debut, taille)],
    )
```