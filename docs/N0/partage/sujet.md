---
author: Nicolas Revéret
title: Partage
tags:
    - 0-simple
    - 1-boucle
status: relecture
---

# Partage d'un tableau

On donne un tableau `valeurs` et un entier `effectif_debut`.

On garantit que `effectif_debut` est un entier de 0 à la longueur de `valeurs`.

Compléter le code de la fonction `partage` qui :

* prend `valeurs` et `effectif_debut` en argument,
* renvoie le couple formé :
    * du tableau comprenant les `effectif_debut` premiers éléments de `valeurs`,
    * du tableau comprenant les éléments restants.

!!! example "Exemples"

    ```pycon
    >>> partage(['pim', 'pam', 'poum'], 2)
    (['pim', 'pam'], ['poum'])
    >>> partage([7, 12, 5, 6, 8], 0)
    ([], [7, 12, 5, 6, 8])
    >>> partage([7, 12, 5, 6, 8], 5)
    ([7, 12, 5, 6, 8], [])
    ```

{{ IDE('exo') }}
