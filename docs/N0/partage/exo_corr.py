def partage(valeurs, effectif_debut):
    longueur = len(valeurs)

    debut_de_valeurs = []
    for i in range(effectif_debut):
        debut_de_valeurs.append(valeurs[i])

    fin_de_valeurs = []
    for i in range(effectif_debut, longueur):
        fin_de_valeurs.append(valeurs[i])

    return (debut_de_valeurs, fin_de_valeurs)


# Tests
assert partage(["pim", "pam", "poum"], 2) == (["pim", "pam"], ["poum"])
assert partage([7, 12, 5, 6, 8], 0) == ([], [7, 12, 5, 6, 8])
assert partage([7, 12, 5, 6, 8], 5) == ([7, 12, 5, 6, 8], [])
