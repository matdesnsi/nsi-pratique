## Commentaires

{{ IDE('exo_corr') }}

### Par compréhension

```pycon
def selection_enclos(refuge, numero_enclos):
    return [ animal['nom'] for animal in refuge if animal['enclos'] == numero_enclos ]
```
