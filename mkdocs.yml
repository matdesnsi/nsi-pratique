site_name: "Travail de création en NSI"
site_url: https://e-nsi.gitlab.io/nsi-pratique
repo_url: https://gitlab.com/e-nsi/nsi-pratique
edit_uri: tree/main/docs/
site_description: Travail de création en NSI
copyright: |
   <p xmlns:cc="http://creativecommons.org/ns#"
   xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title"
   rel="cc:attributionURL"
   href="https://e-nsi.gitlab.io/nsi-pratique/">Exercices pratiques de
   NSI</a> par <a rel="cc:attributionURL dct:creator"
   property="cc:attributionName"
   href="https://gitlab.com/e-nsi/nsi-pratique/">Enseignants de NSI</a>
   partage ou adaptation possible selon les conditions de la
   licence <a
   href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1"
   target="_blank" rel="license noopener noreferrer"
   style="display:inline-block;">CC BY-NC-SA 4.0<img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>
docs_dir: docs

nav:
  - "🏡Accueil": index.md
  - ... | regex=^(?:(?!_REM.md).)*$


theme:
    custom_dir: my_theme_customizations/

    name: material
    font: false                     # RGPD ; pas de fonte Google
    language: fr                    # français
    palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: indigo
        accent: indigo
        toggle:
            icon: material/weather-sunny
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-night
            name: Passer au mode jour
    features:
        - navigation.instant
        - navigation.tabs
        - navigation.top
        - toc.integrate
        - header.autohide


markdown_extensions:
    - meta
    - abbr

    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        auto_title: true
        auto_title_map:
            "Python": "🐍 Script Python"
            "Python Console Session": "🐍 Console Python"
            "Text Only": "📋 Texte"
            "E-mail": "📥 Entrée"
            "Text Output": "📤 Sortie"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
        alternate_style: true 
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index:     !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg


    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format



    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 3

extra:
    social:
        - icon: fontawesome/brands/gitlab
          link: https://gitlab.com/e-nsi
          name: Enseignants de NSI

        - icon: fontawesome/brands/discourse
          link: https://mooc-forums.inria.fr/moocnsi/
          name: Le forum des enseignants de NSI

    site_url: https://e-nsi.gitlab.io/nsi-pratique/


plugins:
  - search
  - awesome-pages:
        collapse_single_pages: true
  - macros
  - tags:
      tags_file: tags.md

extra_javascript:
  - xtra/config.js                    # MathJax
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  - xtra/interpreter.js

extra_css:
  - xtra/pyoditeur.css
  - xtra/ajustements.css                      # ajustements
